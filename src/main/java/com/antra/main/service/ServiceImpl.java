package com.antra.main.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements ArrayServiceInter {

	List<Integer> list = new ArrayList<Integer>();

	public ServiceImpl() {
		list.add(10);
		list.add(20);
		list.add(60);
		list.add(30);
		list.add(50);
		list.add(40);
	}

	@Override
	public List<Integer> displayAll() {
		return list;
	}

	@Override
	public boolean getValue(Integer val) {
		if (list.contains(val))
			return true;
		else
			return false;
	}

	@Override
	public boolean addValue(Integer val) {
		if (list.add(val))
			return true;
		else
			return false;
	}

	@Override
	public boolean updateValue(Integer old, Integer newval) {
		if (list.contains(old)) {
			list.remove(old);
			list.add(newval);
			return true;
		} else
			return false;
	}

	@Override
	public boolean deleteValue(Integer val) {

		if (list.contains(val)) {
			list.remove(val);
			return true;
		} else
			return false;
	}
}