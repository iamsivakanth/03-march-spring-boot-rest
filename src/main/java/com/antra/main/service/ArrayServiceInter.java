package com.antra.main.service;

import java.util.List;

public interface ArrayServiceInter {

	List<Integer> displayAll();

	boolean getValue(Integer val);

	boolean addValue(Integer val);

	boolean updateValue(Integer old, Integer newval);

	boolean deleteValue(Integer val);
}