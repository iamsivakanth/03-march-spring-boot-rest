package com.antra.main.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antra.main.service.ArrayServiceInter;

@RestController
public class ArrayApi {

	@Autowired
	ArrayServiceInter service;

	@GetMapping(value = "/arraylist")
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<List<Integer>>(service.displayAll(), HttpStatus.OK);
	}

	@GetMapping(value = "/arraylist/{val}")
	public ResponseEntity<?> getValue(@PathVariable Integer val) {

		if (service.getValue(val))
			return new ResponseEntity<Integer>(val, HttpStatus.OK);
		else
			return new ResponseEntity<String>("Invalid value", HttpStatus.BAD_REQUEST);
	}

	@PostMapping(value = "/arraylist/add/{val}")
	public ResponseEntity<?> add(@PathVariable Integer val) {
		
		if (service.addValue(val))
			return new ResponseEntity<String>("Value Added", HttpStatus.CREATED);
		else
			return new ResponseEntity<String>("Something went wrong", HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/arraylist/update/{val1}/{val2}")
	public ResponseEntity<?> update(@PathVariable(value = "val1") Integer val1,
			@PathVariable(value = "val2") Integer val2) {
		
		if (service.updateValue(val1, val2))
			return new ResponseEntity<String>("Value Updated", HttpStatus.CREATED);
		else
			return new ResponseEntity<String>("Something went wrong", HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/arraylist/delete/{val}")
	public ResponseEntity<?> delete(@PathVariable Integer val) {
		
		if (service.deleteValue(val))
			return new ResponseEntity<String>("Value Deleted", HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<String>("Something went wrong", HttpStatus.BAD_REQUEST);
	}
}